import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';

import logo from './logo.svg';
import './custom.css'

function App() {
  return (
    <>
      <div>
        <header>
          <Layout>
            <Route exact path='/' component={Home} />
            <Route path='/counter' component={Counter} />
            <Route path='/fetch-data' component={FetchData} />
          </Layout>
        </header>
      </div>
      <img src={logo} className="App-logo" alt="logo" />
      
    </>
  );
}

export default App;
